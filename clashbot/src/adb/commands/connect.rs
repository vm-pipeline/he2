use std::net::Ipv4Addr;
use crate::adb::{AdbTcpConnection};
use crate::adb::models::AdbCommand;

impl AdbTcpConnection {
    /// Connect adb to a device
    pub fn connect(&mut self, address: Ipv4Addr, port: u16) -> crate::adb::Result<()> {
        self.new_connection()?;
        self.proxy_connection(AdbCommand::ConnectDevice(address.to_string(), port.to_string()), false)?;

        Ok(())
    }
}