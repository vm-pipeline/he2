#![allow(dead_code)] // the adb module come from https://github.com/cocool97/adb_client

mod adb_tcp_connection;
mod commands;
mod error;
mod models;
pub use adb_tcp_connection::AdbTcpConnection;
pub use error::{Result, RustADBError};
pub use models::{AdbVersion, Device, DeviceLong, DeviceState};
