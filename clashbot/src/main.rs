use std::process::{Command, Stdio};
use clap::{Parser};
use image::DynamicImage;
use log4rs::init_file;
use log::error;

use clashbot_rs::adb::{AdbTcpConnection, RustADBError};
use clashbot_rs::cli::Cli;
use clashbot_rs::event_handler::EventHandler;

fn main() -> Result<(), RustADBError> {

    init_file("config/log4rs.yaml", Default::default()).unwrap();

    if !is_adb_installed() {
        error!("ADB is not installed");
        return Err(RustADBError::ADBNotFound);
    }
    start_adb_server();

    let opt = Cli::parse();
    let serial = opt.device.serial.unwrap_or(format!("{:?}:{:?}", opt.device.device_ip, opt.device_port));

    let mut adb = AdbTcpConnection::new(opt.address, opt.port).unwrap();

    if opt.pairing_port.is_some() {
        adb.pair(
            opt.code.unwrap(),
            opt.device.device_ip.unwrap(),
            opt.pairing_port.unwrap()
        ).expect("Couldn't pair to the device")
    }

    if opt.device.device_ip.is_some() {
        adb.connect(opt.device.device_ip.unwrap(), opt.device_port.unwrap())
            .expect("Couldn't connect to the device");
    }

    let mut event = EventHandler::new(adb, serial);
    let screenshot: DynamicImage = event.screenshot();
    println!("{}x{}", screenshot.width(), screenshot.height());

    Ok(())
}

fn is_adb_installed() -> bool {
    Command::new("adb").arg("--version").stdout(Stdio::null()).status().is_ok()
}

fn start_adb_server() -> bool {
    Command::new("adb").arg("start-server").stdout(Stdio::null()).status().is_ok()
}
