use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct Config {
    pub ocr: Ocr,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Ocr {
    pub url: String,
    pub auto_update: bool
}
