mod template_matching_ref;
mod config;

pub use template_matching_ref::TemplateMatchingRef;
pub use config::Config;
