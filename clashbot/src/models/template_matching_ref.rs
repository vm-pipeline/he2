pub enum TemplateMatchingRef {
    RedCrossButton,
    UpgradeButton
}

impl TemplateMatchingRef {
    pub fn as_str(&self) -> &str {
        match self {
            TemplateMatchingRef::RedCrossButton => "redcross_button.jpg",
            TemplateMatchingRef::UpgradeButton => "upgrade_button.jpg"
        }
    }
}