use std::path::Path;
use clashbot_rs::image_util::ImageUtil;

fn main() {
    let path = Path::new("/home/valentin/Images/Screenshot_20220302_235414.png");
    let image = image::open(path)
        .unwrap_or_else(|_| panic!("Could not load image at {:?}", path))
        .into_rgb8();

    let mut toto = ImageUtil::new();
    let result = toto.find_text_lines(&image);
    println!("{:?}", result);
}